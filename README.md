## Terminplanungsservice

Based on gitlab spring template which has been updated (SpringBoot: 2.0 -> 2.6.3, Java: 8->11)

### Setup

1. mvn install
2. run Application
3. swagger-ui should then be [here](http://localhost:8080/swagger-ui/index.html#/demo-application/home)

### Tests

Integration test have been implemented for UserStory1: UserControllerTest and for UserStory2: AppointmentControllerTest.
DirtiesContext is used to clear H2 DB between tests as an empty DB is assumed.

### Dependencies

* Spring Booot 2.6.3 with Java 11
* Spring JPA with H2 is used as in-memory DB.
* Lombok is used for simplified Entity generation (getters/setters, Constructor with @Data)
* OpenAPIv3 is used for the api
  * openapi-generator does the controller interface and dto generation
  * springdoc visualizes the API and also simplifies manual testing

### Build

Dockerfile has been kept from the template, as it runs the build.

.gitlab-ci.yml copied from gitlab's Docker template is used to provide basic pipline build on Gitlab.

### Requirements

#### 2.1. User-Story 1 – Bereitstellung einer API zur Verwaltung von Nutzern

Als Entwickler möchte ich die Möglichkeit haben, mittels der bereitgestellten API des Terminplanungsservice, Benutzer
anzulegen, sie zu ändern und zu löschen. Ein Nutzer besteht dabei mindestens aus einem Identifier und einem Namen. In
meiner Anwendung soll es zudem möglich sein, einen bestimmten Benutzer mit allen seiner Nutzerdaten abzufragen. Zudem
ist es weiterhin notwendig auch alle Benutzer auf einmal abzufragen. Auch bei dieser Abfrage sollen alle Nutzerdaten
mitgeschickt werden. Wenn ein Benutzer gelöscht wird, müssen zusätzlich alle Termine, in den der Nutzer eingetragen ist,
angepasst werden und der Nutzer aus diesen Terminen entfernt werden.

#### 2.2. User-Story 2 - Terminplanung

Als Benutzer möchte ich Termine anlegen, ändern und löschen können. Ein Termin besteht dabei immer aus mindestens einem
Nutzer, einem Start- sowie einen Endzeitpunkt und einem Titel. Zudem möchte ich alle Termine mit allen Nutzern sehen
sowie auch nur die Termine eines einzelnen Nutzers abrufen können. Bei der Erstellung eines Termins muss sichergestellt
werden, dass der Termin zu keinen Überlappungen führt. Ein Nutzer kann immer nur einen Termin zu einem Zeitpunkt haben.
Überschneidende Termine sind nicht erlaubt und müssen von der API mit einem entsprechenden Fehler abgelehnt werden.
Dabei soll ersichtlich sein in welchem Zeitraum und mit welchen Terminen es zu Überschneidungen kommt. Beispiel:
Ich möchte einen neuen Termin von 10.01.2022 bis 31.01.2022 anlegen. Da ich aber bereits einen Termin namens 'Urlaub'
von 01.01.2022 bis 15.01.2022 erstellt habe bekomme ich eine Fehlermeldung:
"Es besteht eine Überschneidung mit 'Urlaub' von 10.01.2022 bis 15.01.2022"
