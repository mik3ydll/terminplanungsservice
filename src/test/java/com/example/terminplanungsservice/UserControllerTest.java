package com.example.terminplanungsservice;

import org.junit.jupiter.api.Test;
import org.openapitools.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserControllerTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void contextLoads() {
  }

  @Test
  public void userCRUD() {
    final UserDto user = new UserDto();
    user.setId(UUID.randomUUID());
    user.setName("name");
    //Create
    this.restTemplate.postForObject("/user", user, UserDto.class);
    assertThat(this.restTemplate.getForEntity("/user/" + user.getId(), UserDto.class).getBody()).isEqualTo(user);
    assertThat(getAllUsers()).containsExactly(user);

    //Update
    user.setName("changed");
    this.restTemplate.put("/user/" + user.getId(), user);
    assertThat(getAllUsers()).containsExactly(user);

    //Delete
    this.restTemplate.delete("/user/" + user.getId());
    assertThat(getAllUsers()).isEmpty();
  }

  private List<UserDto> getAllUsers() {
    return this.restTemplate.exchange(
      RequestEntity.get("/users").build(), new ParameterizedTypeReference<List<UserDto>>() {
      }).getBody();
  }

  @Test
  public void check404Errors() {
    final UserDto user = new UserDto();
    user.setId(UUID.randomUUID());
    user.setName("name");
    //Create
    this.restTemplate.postForObject("/user", user, UserDto.class);
    assertThat(this.restTemplate.getForEntity("/user/" + UUID.randomUUID(), UserDto.class).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    //Update
    user.setName("changed");
    assertThat(this.restTemplate.exchange("/user/" + UUID.randomUUID(), HttpMethod.PUT, new HttpEntity<>(user), UserDto.class).getStatusCode())
      .isEqualTo(HttpStatus.NOT_FOUND);

    //Delete
    assertThat(this.restTemplate.exchange(RequestEntity.delete("/user/" + UUID.randomUUID()).build(), UserDto.class).getStatusCode())
      .isEqualTo(HttpStatus.NOT_FOUND);
  }

}
