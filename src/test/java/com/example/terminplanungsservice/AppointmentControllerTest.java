package com.example.terminplanungsservice;

import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.assertj.core.internal.OffsetDateTimeByInstantComparator;
import org.junit.jupiter.api.Test;
import org.openapitools.model.AppointmentDto;
import org.openapitools.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class AppointmentControllerTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void createAppointment() {
    final UserDto user1 = new UserDto();
    user1.setId(UUID.randomUUID());
    user1.setName("name");
    this.restTemplate.postForObject("/user", user1, UserDto.class);
    final UserDto user2 = new UserDto();
    user2.setId(UUID.randomUUID());
    user2.setName("name2");
    this.restTemplate.postForObject("/user", user2, UserDto.class);

    var appointments = List.of(
      newAppointmentDto(List.of(user1), "Urlaub", OffsetDateTime.now().minusDays(5), OffsetDateTime.now().plusDays(5)),
      newAppointmentDto(List.of(user2), "Arzt", OffsetDateTime.now().minusDays(10), OffsetDateTime.now().plusDays(6)),
      newAppointmentDto(List.of(user1, user2), "Privat", OffsetDateTime.now().minusDays(100), OffsetDateTime.now().minusDays(60)),
      newAppointmentDto(List.of(user1), "Irgendwas", OffsetDateTime.now().minusDays(2), OffsetDateTime.now().plusDays(1))
    );

    var configuration = RecursiveComparisonConfiguration.builder().withComparatorForType(OffsetDateTimeByInstantComparator.getInstance(), OffsetDateTime.class).build();
    var response = this.restTemplate.postForEntity("/appointment", appointments.get(0), AppointmentDto.class);

    //test appointment creation & listAll
    appointments.get(0).setId(Objects.requireNonNull(response.getBody()).getId());
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(getAllAppointments()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0)));

    //test overlapping with other user
    response = this.restTemplate.postForEntity("/appointment", appointments.get(1), AppointmentDto.class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    appointments.get(1).setId(Objects.requireNonNull(response.getBody()).getId());
    assertThat(getAllAppointments()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0), appointments.get(1)));

    //test non-overlapping
    response = this.restTemplate.postForEntity("/appointment", appointments.get(2), AppointmentDto.class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    appointments.get(2).setId(Objects.requireNonNull(response.getBody()).getId());
    assertThat(getAllAppointments()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0), appointments.get(1), appointments.get(2)));

    //test overlapping appointment
    response = this.restTemplate.postForEntity("/appointment", appointments.get(3), AppointmentDto.class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
    assertThat(getAllAppointments()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0), appointments.get(1), appointments.get(2)));

    //test listAll by user
    assertThat(restTemplate.exchange(
      RequestEntity.get("/appointments?userId=" + user2.getId()).build(), new ParameterizedTypeReference<List<AppointmentDto>>() {
      }).getBody()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(1), appointments.get(2)));
    assertThat(restTemplate.exchange(
      RequestEntity.get("/appointments?userId=" + user1.getId()).build(), new ParameterizedTypeReference<List<AppointmentDto>>() {
      }).getBody()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0), appointments.get(2)));

    this.restTemplate.delete("/user/" + user2.getId());
    appointments.get(1).setUsers(Collections.emptyList());
    appointments.get(2).setUsers(List.of(user1));
    assertThat(restTemplate.exchange(
      RequestEntity.get("/appointments").build(), new ParameterizedTypeReference<List<AppointmentDto>>() {
      }).getBody()).usingRecursiveComparison(configuration).isEqualTo(List.of(appointments.get(0), appointments.get(1), appointments.get(2)));
    assertThat(restTemplate.exchange(
      RequestEntity.get("/appointments?userId=" + user2.getId()).build(), new ParameterizedTypeReference<List<AppointmentDto>>() {
      }).getBody()).isEmpty();
  }

  private List<AppointmentDto> getAllAppointments() {
    return this.restTemplate.exchange(
      RequestEntity.get("/appointments").build(), new ParameterizedTypeReference<List<AppointmentDto>>() {
      }).getBody();
  }

  private AppointmentDto newAppointmentDto(List<UserDto> users, String title, OffsetDateTime start, OffsetDateTime end) {
    final AppointmentDto appointmentDto = new AppointmentDto();
    appointmentDto.setUsers(users);
    appointmentDto.setTitle(title);
    appointmentDto.setStart(start);
    appointmentDto.setEnd(end);
    return appointmentDto;
  }
}
