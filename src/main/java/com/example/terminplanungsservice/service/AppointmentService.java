package com.example.terminplanungsservice.service;

import com.example.terminplanungsservice.mapper.AppointmentMapper;
import com.example.terminplanungsservice.persistence.model.Appointment;
import com.example.terminplanungsservice.persistence.model.User;
import com.example.terminplanungsservice.persistence.repository.AppointmentRepository;
import com.example.terminplanungsservice.persistence.repository.UserRepository;
import org.joda.time.Interval;
import org.openapitools.model.AppointmentDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AppointmentService {

  private final AppointmentRepository repository;
  private final UserRepository userRepository;
  private final AppointmentMapper mapper;

  public AppointmentService(AppointmentRepository repository, UserRepository userRepository, AppointmentMapper mapper) {
    this.repository = repository;
    this.userRepository = userRepository;
    this.mapper = mapper;
  }

  @Transactional
  public AppointmentDto create(AppointmentDto dto) throws Exception {
    var entity = mapper.toEntity(dto);
    for (User user : entity.getUsers()) {
      var appointments = repository.findAllByUsersContains(user);
      var entityInterval = convertInterval(entity);
      var overlap = appointments.stream().filter(appointment ->
        entityInterval.overlaps(convertInterval(appointment))).findFirst();

      if (overlap.isEmpty()) {
        continue;
      }
      throw new Exception(
        String.format("Es besteht eine Überschneidung mit '%s' von %s bis %s",
          overlap.get().getTitle(),
          overlap.get().getStart().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
          overlap.get().getEnd().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        )
      );
    }
    return mapper.toDto(repository.saveAndFlush(entity));
  }

  private Interval convertInterval(Appointment appointment) {
    return new Interval(appointment.getStart().toEpochSecond(), appointment.getEnd().toEpochSecond());
  }

  public List<AppointmentDto> findAll(UUID userId) throws NoSuchElementException {
    if (userId != null) {
      var appointmentList = repository.findAllByUsersContains(userRepository.getById(userId));
      return appointmentList.stream().map(mapper::toDto).collect(Collectors.toList());
    } else {
      return repository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }
  }
}
