package com.example.terminplanungsservice.service;

import com.example.terminplanungsservice.mapper.UserMapper;
import com.example.terminplanungsservice.persistence.model.Appointment;
import com.example.terminplanungsservice.persistence.model.User;
import com.example.terminplanungsservice.persistence.repository.AppointmentRepository;
import com.example.terminplanungsservice.persistence.repository.UserRepository;
import org.openapitools.model.UserDto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService {

  private final UserRepository userRepository;
  private final AppointmentRepository appointmentRepository;
  private final UserMapper mapper;

  public UserService(UserRepository userRepository, AppointmentRepository appointmentRepository, UserMapper mapper) {
    this.userRepository = userRepository;
    this.appointmentRepository = appointmentRepository;
    this.mapper = mapper;
  }

  public UserDto get(UUID id) throws NoSuchElementException {
    return mapper.toDto(userRepository.findById(id).orElseThrow());
  }

  public List<UserDto> getAll() {
    final Iterable<User> entities = userRepository.findAll();
    return StreamSupport.stream(entities.spliterator(), true)
      .map(mapper::toDto)
      .collect(Collectors.toList());
  }

  @Transactional
  public UserDto update(UserDto dto) {
    final User user = userRepository.saveAndFlush(mapper.toEntity(dto));
    return mapper.toDto(user);
  }

  @Transactional
  public void delete(UUID id) {
    final List<Appointment> appointments = appointmentRepository.findAllByUsersContains(userRepository.getById(id));
    appointments.forEach(appointment ->
      appointment.setUsers(appointment.getUsers().stream().filter(user -> !user.getId().equals(id)).collect(Collectors.toList())));
    appointmentRepository.saveAll(appointments);
    userRepository.deleteById(id);
    userRepository.flush();
  }

}
