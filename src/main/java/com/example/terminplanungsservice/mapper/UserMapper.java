package com.example.terminplanungsservice.mapper;

import com.example.terminplanungsservice.persistence.model.User;
import org.openapitools.model.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

  public UserDto toDto(User entity){
    if(entity != null) {
      var dto = new UserDto();
      dto.setId(entity.getId());
      dto.setName(entity.getName());
      return dto;
    }
    return null;
  }
  public User toEntity(UserDto dto){
    if(dto != null) {
      var entity = new User();
      entity.setId(dto.getId());
      entity.setName(dto.getName());
      return entity;
    }
    return null;
  }
}
