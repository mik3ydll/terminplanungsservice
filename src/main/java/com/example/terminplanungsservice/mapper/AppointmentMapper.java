package com.example.terminplanungsservice.mapper;

import com.example.terminplanungsservice.persistence.model.Appointment;
import org.openapitools.model.AppointmentDto;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class AppointmentMapper {
  private final UserMapper userMapper;

  public AppointmentMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
  }

  public AppointmentDto toDto(Appointment entity) {
    final AppointmentDto dto = new AppointmentDto();
    dto.setId(entity.getId());
    dto.setTitle(entity.getTitle());
    dto.setUsers(entity.getUsers().stream().map(userMapper::toDto).collect(Collectors.toList()));
    dto.setStart(entity.getStart());
    dto.setEnd(entity.getEnd());
    return dto;
  }

  public Appointment toEntity(AppointmentDto dto) {
    final Appointment entity = new Appointment();
    entity.setId(dto.getId());
    entity.setTitle(dto.getTitle());
    entity.setUsers(dto.getUsers().stream().map(userMapper::toEntity).collect(Collectors.toList()));
    entity.setStart(dto.getStart());
    entity.setEnd(dto.getEnd());
    return entity;
  }
}
