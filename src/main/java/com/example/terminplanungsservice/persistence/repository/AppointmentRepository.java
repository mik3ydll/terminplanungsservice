package com.example.terminplanungsservice.persistence.repository;

import com.example.terminplanungsservice.persistence.model.Appointment;
import com.example.terminplanungsservice.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, UUID> {
  List<Appointment> findAllByUsersContains(User user);
}
