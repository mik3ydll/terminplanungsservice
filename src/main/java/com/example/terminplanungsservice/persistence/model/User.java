package com.example.terminplanungsservice.persistence.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
public class User {
  @Id
  private UUID id;

  @Column
  private String name;
}
