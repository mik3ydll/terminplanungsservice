package com.example.terminplanungsservice.persistence.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Data
public class Appointment {
  @Id
  private UUID id;

  private String title;

  @ManyToMany
  private List<User> users;

  private OffsetDateTime start;
  private OffsetDateTime end;
}
