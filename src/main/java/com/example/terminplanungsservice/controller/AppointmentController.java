package com.example.terminplanungsservice.controller;

import com.example.terminplanungsservice.AppointmentApi;
import com.example.terminplanungsservice.AppointmentsApi;
import com.example.terminplanungsservice.service.AppointmentService;
import org.openapitools.model.AppointmentDto;
import org.openapitools.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Controller
public class AppointmentController implements AppointmentApi, AppointmentsApi {
  private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentController.class);

  private final AppointmentService service;

  public AppointmentController(AppointmentService service) {
    this.service = service;
  }

  @Override
  public ResponseEntity create(AppointmentDto appointmentDto) {
    try {
      appointmentDto.setId(UUID.randomUUID());
      return ResponseEntity.ok(service.create(appointmentDto));
    } catch (Exception e) {
      LOGGER.warn(e.getMessage());
      var error = new Error();
      error.setMessage(e.getMessage());
      return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
    }
  }

  @Override
  public ResponseEntity<List<AppointmentDto>> get(UUID userId) {
    try {
      return ResponseEntity.ok(service.findAll(userId));
    } catch (NoSuchElementException noSuchElementException) {
      LOGGER.warn(noSuchElementException.getMessage(), noSuchElementException);
      return ResponseEntity.notFound().build();
    }
  }
}
