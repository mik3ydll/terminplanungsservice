package com.example.terminplanungsservice.controller;

import com.example.terminplanungsservice.UserApi;
import com.example.terminplanungsservice.UsersApi;
import com.example.terminplanungsservice.service.UserService;
import org.openapitools.model.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Controller
public class UserController implements UserApi, UsersApi {

  private final UserService service;

  public UserController(UserService service) {
    this.service = service;
  }

  @Override
  public ResponseEntity<List<UserDto>> getAll() {
    return ResponseEntity.ok(service.getAll());
  }

  @Override
  public ResponseEntity<UserDto> create(UserDto userDto) {
    return ResponseEntity.ok(service.update(userDto));
  }

  @Override
  public ResponseEntity<Void> delete(UUID id) {
    try {
      service.get(id);
    } catch (NoSuchElementException noSuchElementException) {
      return ResponseEntity.notFound().build();
    }
    service.delete(id);
    return ResponseEntity.ok().build();

  }

  @Override
  public ResponseEntity<UserDto> get(UUID id) {
    try {
      return ResponseEntity.ok(service.get(id));
    } catch (NoSuchElementException noSuchElementException) {
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity<UserDto> update(UUID id, UserDto userDto) {
    try {
      service.get(id);
    } catch (NoSuchElementException noSuchElementException) {
      return ResponseEntity.notFound().build();
    }
    userDto.setId(id);
    return ResponseEntity.ok(service.update(userDto));

  }
}
